package ru.t1.shevyreva.tm.exception.field;

public class PasswordEmptyException extends AbsrtactFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty.");
    }

}
