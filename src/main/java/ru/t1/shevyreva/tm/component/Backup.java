package ru.t1.shevyreva.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.command.data.AbstractDataCommand;
import ru.t1.shevyreva.tm.command.data.DataBinaryLoadCommand;
import ru.t1.shevyreva.tm.command.data.DataBinarySaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() {
        load();
        start();
    }

    public void save() {
        bootstrap.processCommand(DataBinarySaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY))) return;
        bootstrap.processCommand(DataBinaryLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }

}
